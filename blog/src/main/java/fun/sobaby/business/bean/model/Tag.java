package fun.sobaby.business.bean.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import fun.sobaby.business.bean.enums.LevelEnum;

/**
 * 数据库表[tag]的数据模型
 * 
 * 
 * 
 * @author 最爱吃小鱼
 * 
 */

@TableName("tag")
@SuppressWarnings("serial")
public class Tag implements Serializable {

	@TableId
	private Long id;
    // 父标签ID，-1表示一级标签 
    @TableField("parent_id")
    private Long parentId;
    // 标签名称 
    @TableField("name")
    private String name;
    // 标签层级 
    @TableField("level")
    private LevelEnum level;
    
    /** 默认构造函数 */
    public Tag() {
    }
    
    /** 全参构造函数 */
    public Tag(Long parentId, String name, LevelEnum level) {
      this.parentId = parentId;
      this.name = name;
      this.level = level;
    }
     
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
    public  Long getParentId() {
        return this.parentId;
    }
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
    public  String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public  LevelEnum getLevel() {
        return this.level;
    }
    public void setLevel(LevelEnum level) {
        this.level = level;
    }
}
