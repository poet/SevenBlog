package fun.sobaby.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import fun.sobaby.business.bean.model.Contact;
import fun.sobaby.business.mapper.ContactMapper;
import fun.sobaby.business.service.ContactService;
import org.springframework.stereotype.Service;

/**
 * 
 * @author 最爱吃小鱼
 *
 */
@Service
public class ContactServiceImpl extends ServiceImpl<ContactMapper, Contact> implements ContactService {

	// 方法里面使用 baseMapper 调用
}
