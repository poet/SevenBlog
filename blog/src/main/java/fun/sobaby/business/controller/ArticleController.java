package fun.sobaby.business.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import fun.sobaby.business.bean.dto.ArticleDTO;
import fun.sobaby.business.bean.vo.ArticleVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fun.sobaby.business.bean.model.Article;
import fun.sobaby.business.service.ArticleService;

/**
 * 文章的维护
 * 
 * @author 最爱吃小鱼
 *
 */
@RestController
public class ArticleController {

	@Autowired
	private ArticleService articleService;

	// 文章的列表
	@RequestMapping("/article/list")
	public IPage<Article> list(Page page) {
		return articleService.page(page);
	}

	// 文章的编辑
	@RequestMapping("/article/edit")
	public void edit(@RequestBody ArticleDTO record) {
		articleService.edit(record);
	}

	// 文章的详情
	@RequestMapping("/article/get")
	public ArticleVO detail(Long id) {
		return articleService.detail(id);
	}

	// 文章的删除
	@RequestMapping("/article/remove")
	public void remove(Long id) {
		articleService.removeArticle(id);
	}
	
}
