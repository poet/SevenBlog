package fun.sobaby.business.bean.dto;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author 最爱吃小鱼
 */
public class ResumeWorkExperienceDTO implements Serializable {

    /**
     * 公司LOGO
     */
    @NotBlank
    private String companyLogo;
    /**
     * 公司名称
     */
    @NotBlank
    private String companyName;
    /**
     * 公司网址
     */
    @NotBlank
    private String companyWebsite;
    /**
     * 职位
     */
    @NotBlank
    private String jobName;
    /**
     * 工作时间
     */
    @NotBlank
    private String workTimes;
    /**
     * 工作内容
     */
    @NotBlank
    private String workContent;

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyWebsite() {
        return companyWebsite;
    }

    public void setCompanyWebsite(String companyWebsite) {
        this.companyWebsite = companyWebsite;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getWorkTimes() {
        return workTimes;
    }

    public void setWorkTimes(String workTimes) {
        this.workTimes = workTimes;
    }

    public String getWorkContent() {
        return workContent;
    }

    public void setWorkContent(String workContent) {
        this.workContent = workContent;
    }
}
