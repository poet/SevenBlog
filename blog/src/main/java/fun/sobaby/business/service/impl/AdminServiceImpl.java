package fun.sobaby.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import fun.sobaby.business.bean.model.Admin;
import fun.sobaby.business.mapper.AdminMapper;
import fun.sobaby.business.service.AdminService;
import fun.sobaby.core.enums.CodeEnum;
import fun.sobaby.core.exception.BusinessException;
import org.apache.commons.codec.digest.Md5Crypt;
import org.springframework.stereotype.Service;

/**
 * 
 * @author 最爱吃小鱼
 *
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {

    @Override
    public void checkLogin(String username, String password) {

        Admin admin = baseMapper.selectOne(new QueryWrapper<Admin>().eq("username", username));
        if (admin == null) {
            throw new BusinessException(CodeEnum.ERR_ADMIN_NONE);
        }
        if (!admin.getPassword().equals(Md5Crypt.md5Crypt(password.getBytes(), admin.getSalt()))) {
            throw new BusinessException(CodeEnum.ERR_PASSWORD);
        }

    }

    // 方法里面使用 baseMapper 调用
}
