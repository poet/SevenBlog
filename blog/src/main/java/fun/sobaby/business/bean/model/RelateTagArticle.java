package fun.sobaby.business.bean.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 数据库表[relate_tag_article]的数据模型
 * 标签与文章关联表
 * 
 * 
 * @author 最爱吃小鱼
 * 
 */

@TableName("relate_tag_article")
@SuppressWarnings("serial")
public class RelateTagArticle implements Serializable {

	@TableId
	private Long id;
    // 标签ID 
    @TableField("tag_id")
    private Long tagId;
    // 文章ID 
    @TableField("article_id")
    private Long articleId;
    
    /** 默认构造函数 */
    public RelateTagArticle() {
    }
    
    /** 全参构造函数 */
    public RelateTagArticle(Long tagId, Long articleId) {
      this.tagId = tagId;
      this.articleId = articleId;
    }
     
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
    public  Long getTagId() {
        return this.tagId;
    }
    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }
    public  Long getArticleId() {
        return this.articleId;
    }
    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }
}
