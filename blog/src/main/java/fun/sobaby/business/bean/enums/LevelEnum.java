package fun.sobaby.business.bean.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * @author 最爱吃小鱼
 */
public enum LevelEnum {

    ONE(1, "一级标签"), TWO(2, "二级标签");

    @EnumValue
    private Integer value;

    private String describe;


    LevelEnum(Integer value, String describe) {
        this.value = value;
        this.describe = describe;
    }

    @JsonCreator
    public static LevelEnum of(String value) {
        for (LevelEnum levelEnum : values()) {
            if (levelEnum.value.equals(value)) {
                return levelEnum;
            }
        }
        return null;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }
}
