package fun.sobaby.business.bean.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import fun.sobaby.business.bean.enums.ShowStatusEnum;

import java.io.Serializable;

/**
 * 数据库表[comment]的数据模型
 * 评论
 * 
 * 
 * @author 最爱吃小鱼
 * 
 */

@TableName("comment")
@SuppressWarnings("serial")
public class Comment implements Serializable {

	@TableId
	private Long id;
    // 昵称 
    @TableField("nick_name")
    private String nickName;
    // 内容 
    @TableField("content")
    private String content;
    // 是否显示 0显示，1显示 
    @TableField("status")
    private ShowStatusEnum status;
    // 创建时间 
    @TableField("create_time")
    private java.util.Date createTime;
    
    /** 默认构造函数 */
    public Comment() {
    }
    
    /** 全参构造函数 */
    public Comment(String nickName, String content, ShowStatusEnum status, java.util.Date createTime) {
      this.nickName = nickName;
      this.content = content;
      this.status = status;
      this.createTime = createTime;
    }
     
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
    public  String getNickName() {
        return this.nickName;
    }
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
    public  String getContent() {
        return this.content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public ShowStatusEnum getStatus() {
        return status;
    }
    public void setStatus(ShowStatusEnum status) {
        this.status = status;
    }
    public  java.util.Date getCreateTime() {
        return this.createTime;
    }
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }
}
