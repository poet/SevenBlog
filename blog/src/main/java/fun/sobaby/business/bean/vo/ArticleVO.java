package fun.sobaby.business.bean.vo;

import fun.sobaby.business.bean.model.Article;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.List;

/**
 * @author 最爱吃小鱼
 */
public class ArticleVO implements Serializable {
    /**
     * ID
     */
    private Long id;
    /**
     * 封面图片
     */
    private String coverImg;
    /**
     * 标题
     */
    private String title;
    /**
     * 内容
     */
    private String content;

    /**
     * 标签
     */
    private List<TagVO> tags;

    /**
     * 初始化
     *
     * @param article
     * @param tagVOList
     * @return
     */
    public static ArticleVO init(Article article, List<TagVO> tagVOList) {
        ArticleVO articleVO = new ArticleVO();
        BeanUtils.copyProperties(article, articleVO);
        articleVO.setTags(tagVOList);
        return articleVO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCoverImg() {
        return coverImg;
    }

    public void setCoverImg(String coverImg) {
        this.coverImg = coverImg;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<TagVO> getTags() {
        return tags;
    }

    public void setTags(List<TagVO> tags) {
        this.tags = tags;
    }
}
