package fun.sobaby.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import fun.sobaby.business.bean.model.ResumeWorkExperience;

/**
 * 
 * @author 最爱吃小鱼
 *
 */
public interface ResumeWorkExperienceService extends IService<ResumeWorkExperience>{

}
