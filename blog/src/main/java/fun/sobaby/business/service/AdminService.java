package fun.sobaby.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import fun.sobaby.business.bean.model.Admin;

/**
 * 
 * @author 最爱吃小鱼
 *
 */
public interface AdminService extends IService<Admin> {

    /**
     * 取得登录的token
     *
     * @param username
     * @param password
     * @return 字符串token
     */
    void checkLogin(String username, String password);
}
