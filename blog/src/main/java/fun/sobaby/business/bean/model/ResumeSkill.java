package fun.sobaby.business.bean.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 数据库表[resume_skill]的数据模型
 * 技能
 * 
 * 
 * @author 最爱吃小鱼
 * 
 */

@TableName("resume_skill")
@SuppressWarnings("serial")
public class ResumeSkill implements Serializable {

	@TableId
	private Long id;
    // 简历ID 
    @TableField("resume_id")
    private Long resumeId;
    // 0自然语言
    //1机器语言
    //2其它
    @TableField("type")
    private Integer type;
    // 技能名称 
    @TableField("name")
    private String name;
    // 掌握度：0~100以内的整数 
    @TableField("master")
    private Integer master;
    
    /** 默认构造函数 */
    public ResumeSkill() {
    }
    
    /** 全参构造函数 */
    public ResumeSkill(Long resumeId, Integer type, String name, Integer master) {
      this.resumeId = resumeId;
      this.type = type;
      this.name = name;
      this.master = master;
    }
     
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
    public  Long getResumeId() {
        return this.resumeId;
    }
    public void setResumeId(Long resumeId) {
        this.resumeId = resumeId;
    }
    public  Integer getType() {
        return this.type;
    }
    public void setType(Integer type) {
        this.type = type;
    }
    public  String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public  Integer getMaster() {
        return this.master;
    }
    public void setMaster(Integer master) {
        this.master = master;
    }
}
