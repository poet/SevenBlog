package fun.sobaby.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import fun.sobaby.business.bean.model.ResumePrize;
import fun.sobaby.business.mapper.ResumePrizeMapper;
import fun.sobaby.business.service.ResumePrizeService;
import org.springframework.stereotype.Service;

/**
 * 
 * @author 最爱吃小鱼
 *
 */
@Service
public class ResumePrizeServiceImpl extends ServiceImpl<ResumePrizeMapper, ResumePrize> implements ResumePrizeService {

	// 方法里面使用 baseMapper 调用
}
