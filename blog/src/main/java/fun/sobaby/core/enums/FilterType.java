package fun.sobaby.core.enums;

/**
 * @author 最爱吃小鱼
 */
public enum FilterType {
    INCLUDE, EXCLUDE
}
