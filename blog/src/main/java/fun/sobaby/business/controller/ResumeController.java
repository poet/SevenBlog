package fun.sobaby.business.controller;

import fun.sobaby.business.bean.dto.ResumeDetailDTO;
import fun.sobaby.business.bean.model.Resume;
import fun.sobaby.business.bean.vo.ResumeVO;
import fun.sobaby.business.service.ResumeService;
import fun.sobaby.core.annotations.AttrFilter;
import fun.sobaby.core.enums.FilterType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 简历的维护
 * 
 * @author 最爱吃小鱼
 *
 */
@Validated
@RestController
public class ResumeController {

	@Autowired
	private ResumeService resumeService;


	/**
	 * 简历的编辑
	 *
	 * @param record 简历
	 */
	@RequestMapping("/resume/edit")
	public void edit(@Valid @RequestBody ResumeDetailDTO record) {
		resumeService.saveOrUpdateResume(record);
	}

	/**
	 * 取得简历信息
	 *
	 * @return
	 */
	@AttrFilter(clazz = Resume.class, type = FilterType.EXCLUDE, value = {"updateTime"})
	@RequestMapping("/resume/get")
	public ResumeVO detail() {
		return resumeService.selectDetail();
	}

}
