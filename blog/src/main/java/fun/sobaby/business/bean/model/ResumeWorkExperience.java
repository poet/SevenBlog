package fun.sobaby.business.bean.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 数据库表[resume_work_experience]的数据模型
 * 工作经历
 * 
 * 
 * @author 最爱吃小鱼
 * 
 */

@TableName("resume_work_experience")
@SuppressWarnings("serial")
public class ResumeWorkExperience implements Serializable {

	@TableId
	private Long id;
    // 简历ID 
    @TableField("resume_id")
    private Long resumeId;
    // 公司LOGO 
    @TableField("company_logo")
    private String companyLogo;
    // 公司名称 
    @TableField("company_name")
    private String companyName;
    // 公司网址 
    @TableField("company_website")
    private String companyWebsite;
    // 职位 
    @TableField("job_name")
    private String jobName;
    // 工作时间 
    @TableField("work_times")
    private String workTimes;
    // 工作内容 
    @TableField("work_content")
    private String workContent;
    
    /** 默认构造函数 */
    public ResumeWorkExperience() {
    }
    
    /** 全参构造函数 */
    public ResumeWorkExperience(Long resumeId, String companyLogo, String companyName, String companyWebsite, String jobName, String workTimes, String workContent) {
      this.resumeId = resumeId;
      this.companyLogo = companyLogo;
      this.companyName = companyName;
      this.companyWebsite = companyWebsite;
      this.jobName = jobName;
      this.workTimes = workTimes;
      this.workContent = workContent;
    }
     
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
    public  Long getResumeId() {
        return this.resumeId;
    }
    public void setResumeId(Long resumeId) {
        this.resumeId = resumeId;
    }
    public  String getCompanyLogo() {
        return this.companyLogo;
    }
    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }
    public  String getCompanyName() {
        return this.companyName;
    }
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    public  String getCompanyWebsite() {
        return this.companyWebsite;
    }
    public void setCompanyWebsite(String companyWebsite) {
        this.companyWebsite = companyWebsite;
    }
    public  String getJobName() {
        return this.jobName;
    }
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
    public  String getWorkTimes() {
        return this.workTimes;
    }
    public void setWorkTimes(String workTimes) {
        this.workTimes = workTimes;
    }
    public  String getWorkContent() {
        return this.workContent;
    }
    public void setWorkContent(String workContent) {
        this.workContent = workContent;
    }
}
