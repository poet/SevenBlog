package fun.sobaby.business.bean.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 数据库表[resume_prize]的数据模型
 * 简历-奖项
 * 
 * 
 * @author 最爱吃小鱼
 * 
 */

@TableName("resume_prize")
@SuppressWarnings("serial")
public class ResumePrize implements Serializable {

	@TableId
	private Long id;
    // 简历ID 
    @TableField("resume_id")
    private Long resumeId;
    // 图片 
    @TableField("cover_img")
    private String coverImg;
    // 奖项名称 
    @TableField("title")
    private String title;
    // 描述 
    @TableField("description")
    private String description;
    
    /** 默认构造函数 */
    public ResumePrize() {
    }
    
    /** 全参构造函数 */
    public ResumePrize(Long resumeId, String coverImg, String title, String description) {
      this.resumeId = resumeId;
      this.coverImg = coverImg;
      this.title = title;
      this.description = description;
    }
     
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
    public  Long getResumeId() {
        return this.resumeId;
    }
    public void setResumeId(Long resumeId) {
        this.resumeId = resumeId;
    }
    public  String getCoverImg() {
        return this.coverImg;
    }
    public void setCoverImg(String coverImg) {
        this.coverImg = coverImg;
    }
    public  String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public  String getDescription() {
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
}
