package fun.sobaby;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "fun.sobaby")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
