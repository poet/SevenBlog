package fun.sobaby.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import fun.sobaby.business.bean.model.Comment;

/**
 * 
 * @author 最爱吃小鱼
 *
 */
public interface CommentService extends IService<Comment>{

}
