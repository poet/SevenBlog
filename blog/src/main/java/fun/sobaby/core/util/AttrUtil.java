package fun.sobaby.core.util;

import fun.sobaby.core.annotations.AttrFilter;
import fun.sobaby.core.annotations.AttrFilters;
import fun.sobaby.core.bean.DataResponse;
import fun.sobaby.core.enums.CodeEnum;
import fun.sobaby.core.enums.FilterType;
import fun.sobaby.core.exception.BusinessException;
import org.apache.commons.lang3.ArrayUtils;

import java.lang.reflect.Method;
import java.util.*;

/**
 * 保存@AttrFilter @AttrInclude注解的值
 *
 * @author 最爱吃小鱼
 */
@SuppressWarnings("AlibabaThreadLocalShouldRemove")
public class AttrUtil {

    private static final ThreadLocal<List<AttrFilter>> ATTR_MAP = new ThreadLocal<>();

    /**
     * 将注解的值保存在本地线程中
     *
     * @param method
     */
    public static void process(Method method) {
        AttrFilter attrFilter = method.getAnnotation(AttrFilter.class);
        AttrFilters attrFilters = method.getAnnotation(AttrFilters.class);
        if (attrFilter == null && attrFilters == null) {
            return;
        }
        List<AttrFilter> attrFilterList = new ArrayList<>();
        if (attrFilter != null) {
            attrFilterList.add(attrFilter);
        } else {
            attrFilterList.addAll(Arrays.asList(attrFilters.value()));
            attrFilterList.forEach(af -> {
                if (af.clazz() == AttrFilter.ALL.class) {
                    throw new BusinessException(CodeEnum.ERR_ATTR);
                }
            });
        }
        ATTR_MAP.set(attrFilterList);
    }

    /**
     * 属性过滤
     *
     * @param object
     * @param name
     * @return
     */
    public static boolean filter(Object object, String name) {
        if (object.getClass() == DataResponse.class) {
            return true;
        }
        if (ATTR_MAP.get() == null) {
            return true;
        }
        for (AttrFilter attrFilter : ATTR_MAP.get()) {
            if (object.getClass() == attrFilter.clazz() || attrFilter.clazz() == AttrFilter.ALL.class) {
                if (attrFilter.type() == FilterType.INCLUDE) {
                    return ArrayUtils.contains(attrFilter.value(), name);
                }
                if (attrFilter.type() == FilterType.EXCLUDE) {
                    return !ArrayUtils.contains(attrFilter.value(), name);
                }
            }
        }
        return true;
    }
}
