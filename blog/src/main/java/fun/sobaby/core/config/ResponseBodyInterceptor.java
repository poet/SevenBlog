package fun.sobaby.core.config;

import fun.sobaby.core.annotations.Original;
import fun.sobaby.core.bean.DataResponse;
import fun.sobaby.core.util.AttrUtil;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * controller拦截器，重写返回值
 * 如果方法有@Original注解则跳过
 *
 * @author 最爱吃小鱼
 *
 */
@ControllerAdvice
public class ResponseBodyInterceptor implements ResponseBodyAdvice<Object> {

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		AttrUtil.process(returnType.getMethod());
		return returnType.getMethod().getAnnotation(Original.class) == null;
	}

	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
                                  Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
                                  ServerHttpResponse response) {
		return DataResponse.of(body);
	}
}
