package fun.sobaby.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import fun.sobaby.business.bean.model.ResumeWorkExperience;
import fun.sobaby.business.mapper.ResumeWorkExperienceMapper;
import fun.sobaby.business.service.ResumeWorkExperienceService;
import org.springframework.stereotype.Service;

/**
 * 
 * @author 最爱吃小鱼
 *
 */
@Service
public class ResumeWorkExperienceServiceImpl extends ServiceImpl<ResumeWorkExperienceMapper, ResumeWorkExperience> implements ResumeWorkExperienceService {

	// 方法里面使用 baseMapper 调用
}
