package fun.sobaby.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import fun.sobaby.business.bean.model.ResumeProjectExperience;
import fun.sobaby.business.mapper.ResumeProjectExperienceMapper;
import fun.sobaby.business.service.ResumeProjectExperienceService;
import org.springframework.stereotype.Service;

/**
 * 
 * @author 最爱吃小鱼
 *
 */
@Service
public class ResumeProjectExperienceServiceImpl extends ServiceImpl<ResumeProjectExperienceMapper, ResumeProjectExperience> implements ResumeProjectExperienceService {

	// 方法里面使用 baseMapper 调用
}
