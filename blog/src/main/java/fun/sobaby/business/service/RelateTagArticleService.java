package fun.sobaby.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import fun.sobaby.business.bean.model.RelateTagArticle;

/**
 * 
 * @author 最爱吃小鱼
 *
 */
public interface RelateTagArticleService extends IService<RelateTagArticle>{

}
