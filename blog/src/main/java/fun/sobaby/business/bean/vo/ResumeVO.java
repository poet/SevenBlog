package fun.sobaby.business.bean.vo;

import fun.sobaby.business.bean.model.*;

import java.io.Serializable;
import java.util.List;

/**
 * @author 最爱吃小鱼
 */
public class ResumeVO implements Serializable {

    /**
     * 基本的简历信息
     */
    private Resume resume;

    /**
     * 技能
     */
    private List<ResumeSkill> skills;

    /**
     * 项目经历
     */
    private List<ResumeProjectExperience> projectExperiences;

    /**
     * 工作经历
     */
    private List<ResumeWorkExperience> workExperiences;

    /**
     * 奖项列表
     */
    private List<ResumePrize> prizes;

    public ResumeVO() {
    }

    public ResumeVO(Resume resume, List<ResumeSkill> skills, List<ResumePrize> prizes, List<ResumeWorkExperience> workExperiences, List<ResumeProjectExperience> projectExperiences) {
        this.resume = resume;
        this.skills = skills;
        this.prizes = prizes;
        this.workExperiences = workExperiences;
        this.projectExperiences = projectExperiences;
    }

    public Resume getResume() {
        return resume;
    }

    public void setResume(Resume resume) {
        this.resume = resume;
    }

    public List<ResumeSkill> getSkills() {
        return skills;
    }

    public void setSkills(List<ResumeSkill> skills) {
        this.skills = skills;
    }

    public List<ResumeProjectExperience> getProjectExperiences() {
        return projectExperiences;
    }

    public void setProjectExperiences(List<ResumeProjectExperience> projectExperiences) {
        this.projectExperiences = projectExperiences;
    }

    public List<ResumeWorkExperience> getWorkExperiences() {
        return workExperiences;
    }

    public void setWorkExperiences(List<ResumeWorkExperience> workExperiences) {
        this.workExperiences = workExperiences;
    }

    public List<ResumePrize> getPrizes() {
        return prizes;
    }

    public void setPrizes(List<ResumePrize> prizes) {
        this.prizes = prizes;
    }
}



