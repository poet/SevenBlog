package fun.sobaby.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import fun.sobaby.business.bean.dto.ResumeDetailDTO;
import fun.sobaby.business.bean.model.*;
import fun.sobaby.business.bean.vo.ResumeVO;
import fun.sobaby.business.mapper.ResumeMapper;
import fun.sobaby.business.service.*;
import fun.sobaby.core.enums.CodeEnum;
import fun.sobaby.core.exception.BusinessException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 
 * @author 最爱吃小鱼
 *
 */
@Service
public class ResumeServiceImpl extends ServiceImpl<ResumeMapper, Resume> implements ResumeService {

    @Autowired
    ResumeSkillService resumeSkillService;

    @Autowired
    ResumePrizeService resumePrizeService;

    @Autowired
    ResumeWorkExperienceService resumeWorkExperienceService;

    @Autowired
    ResumeProjectExperienceService resumeProjectExperienceService;

    @Override
    public ResumeVO selectDetail() {

        Resume resume =  baseMapper.selectOne(null);
        if (resume == null) {
            throw new BusinessException(CodeEnum.ERR_RESUME_NONE);
        }
        List<ResumeSkill> skills = resumeSkillService.list(new QueryWrapper<ResumeSkill>().eq("resume_id", resume.getId()));
        List<ResumePrize> prizes = resumePrizeService.list(new QueryWrapper<ResumePrize>().eq("resume_id", resume.getId()));
        List<ResumeWorkExperience> workExperiences = resumeWorkExperienceService.list(new QueryWrapper<ResumeWorkExperience>().eq("resume_id", resume.getId()));
        List<ResumeProjectExperience> projectExperiences = resumeProjectExperienceService.list(new QueryWrapper<ResumeProjectExperience>().eq("resume_id", resume.getId()));
        return new ResumeVO(resume, skills, prizes, workExperiences, projectExperiences);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveOrUpdateResume(ResumeDetailDTO record) {
        remove(null);
        resumeSkillService.remove(null);
        resumePrizeService.remove(null);
        resumeWorkExperienceService.remove(null);
        resumeProjectExperienceService.remove(null);

        Resume resume =  new Resume();
        BeanUtils.copyProperties(record.getResume(), resume);
        save(resume);

        List<ResumeSkill> skills = new ArrayList<>();
        record.getNatureLanguages().forEach(n -> {
            ResumeSkill skill = new ResumeSkill();
            BeanUtils.copyProperties(n,skill);
            skill.setResumeId(resume.getId());
            skills.add(skill);
        });
        record.getMachineLanguages().forEach(n -> {
            ResumeSkill skill = new ResumeSkill();
            BeanUtils.copyProperties(n,skill);
            skill.setResumeId(resume.getId());
            skills.add(skill);
        });
        record.getOthers().forEach(n -> {
            ResumeSkill skill = new ResumeSkill();
            BeanUtils.copyProperties(n,skill);
            skill.setResumeId(resume.getId());
            skills.add(skill);
        });
        resumeSkillService.saveBatch(skills);

        List<ResumePrize> prizes = record.getPrizes().stream().map(d -> {
            ResumePrize prize = new ResumePrize();
            BeanUtils.copyProperties(d, prize);
            prize.setResumeId(resume.getId());
            return prize;
        }).collect(Collectors.toList());
        resumePrizeService.saveBatch(prizes);

        List<ResumeWorkExperience> workExperiences = record.getWorkExperiences().stream().map(d -> {
            ResumeWorkExperience workExperience = new ResumeWorkExperience();
            BeanUtils.copyProperties(d, workExperience);
            workExperience.setResumeId(resume.getId());
            return workExperience;
        }).collect(Collectors.toList());
        resumeWorkExperienceService.saveBatch(workExperiences);

        List<ResumeProjectExperience> projectExperiences = record.getProjectExperiences().stream().map(d -> {
            ResumeProjectExperience projectExperience = new ResumeProjectExperience();
            BeanUtils.copyProperties(d, projectExperience);
            projectExperience.setResumeId(resume.getId());
            return projectExperience;
        }).collect(Collectors.toList());
        resumeProjectExperienceService.saveBatch(projectExperiences);
    }

    // 方法里面使用 baseMapper 调用
}
