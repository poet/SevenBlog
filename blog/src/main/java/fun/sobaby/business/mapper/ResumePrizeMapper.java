package fun.sobaby.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import fun.sobaby.business.bean.model.ResumePrize;
/**
 * 
 * @author 最爱吃小鱼
 *
 */
public interface ResumePrizeMapper extends BaseMapper<ResumePrize> {

}
