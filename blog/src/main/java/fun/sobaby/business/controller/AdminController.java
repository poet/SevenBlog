package fun.sobaby.business.controller;

import fun.sobaby.business.service.AdminService;
import fun.sobaby.core.enums.CommonEnum;
import fun.sobaby.core.enums.Unimpeded;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotBlank;

/**
 * 管理账号的维护
 * 
 * @author 最爱吃小鱼
 *
 */
@RestController
public class AdminController {

	@Autowired
	private AdminService adminService;

	/**
	 * 登录
	 *
	 * @param username
	 * @param password
	 * @return token 访问后台接口权限token
	 */
	@Unimpeded
	@GetMapping("/login")
	public void login(HttpSession session, @NotBlank String username, @NotBlank String password) {
		adminService.checkLogin(username, password);
		session.setAttribute(CommonEnum.LOG_SESSION_KEY.getKey(), username);
	}

	/**
	 * 登出
	 *
	 * @param session
	 */
	@Unimpeded
	@GetMapping("/logout")
	public void logout(HttpSession session) {
		session.removeAttribute(CommonEnum.LOG_SESSION_KEY.getKey());
	}


}
