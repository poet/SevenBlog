package fun.sobaby.business.bean.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;


/**
 * 数据库表[admin]的数据模型
 * 管理账号
 * 
 * 
 * @author 最爱吃小鱼
 * 
 */

@TableName("admin")
@SuppressWarnings("serial")
public class Admin implements Serializable {

	@TableId
	private Long id;
    // 账号 
    @TableField("username")
    private String username;
    // 密码
    @TableField("password")
    private String password;
    // 随机盐 
    @TableField("salt")
    private String salt;
    
    /** 默认构造函数 */
    public Admin() {
    }
    
    /** 全参构造函数 */
    public Admin(String username, String password, String salt) {
      this.username = username;
      this.password = password;
      this.salt = salt;
    }
     
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
    public  String getUsername() {
        return this.username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public  String getPassword() {
        return this.password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public  String getSalt() {
        return this.salt;
    }
    public void setSalt(String salt) {
        this.salt = salt;
    }
}
