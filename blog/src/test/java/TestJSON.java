import org.junit.Test;

/**
 * @author 最爱吃小鱼
 */
public class TestJSON {

    private static final ThreadLocal<Integer> IT = new ThreadLocal<>();

    @Test
    public void test() {
    }

    @Test
    public void test1() {
        for (int i = 0 ; i < 100 ; i++) {
            final int j = i;
            new Thread(() -> {
                IT.set(j);
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(IT.get());
            }).start();
        }

        try {
            Thread.sleep(10000000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
