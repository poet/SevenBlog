package fun.sobaby.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import fun.sobaby.business.bean.model.Tag;
import fun.sobaby.business.bean.vo.TagVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * @author 最爱吃小鱼
 *
 */
public interface TagMapper extends BaseMapper<Tag> {

    /**
     * 查询文章对应的标签
     *
     * @param articleId
     * @return
     */
    List<TagVO> selectTagsByArticleId(@Param("articleId") Long articleId);
}
