package fun.sobaby.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import fun.sobaby.business.bean.model.ResumeSkill;
/**
 * 
 * @author 最爱吃小鱼
 *
 */
public interface ResumeSkillMapper extends BaseMapper<ResumeSkill> {

}
