package fun.sobaby.business.bean.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 数据库表[article]的数据模型
 * 文章
 * 
 * 
 * @author 最爱吃小鱼
 * 
 */

@TableName("article")
@SuppressWarnings("serial")
public class Article implements Serializable {

	@TableId
	private Long id;
    // 封面图片 
    @TableField("cover_img")
    private String coverImg;
    // 标题 
    @TableField("title")
    private String title;
    // 内容 
    @TableField("content")
    private String content;
    // 创建时间 
    @TableField("create_time")
    private java.util.Date createTime;
    // 更新时间 
    @TableField("update_time")
    private java.util.Date updateTime;
    
    /** 默认构造函数 */
    public Article() {
    }
    
    /** 全参构造函数 */
    public Article(String coverImg, String title, String content, java.util.Date createTime, java.util.Date updateTime) {
      this.coverImg = coverImg;
      this.title = title;
      this.content = content;
      this.createTime = createTime;
      this.updateTime = updateTime;
    }
     
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
    public  String getCoverImg() {
        return this.coverImg;
    }
    public void setCoverImg(String coverImg) {
        this.coverImg = coverImg;
    }
    public  String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public  String getContent() {
        return this.content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public  java.util.Date getCreateTime() {
        return this.createTime;
    }
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }
    public  java.util.Date getUpdateTime() {
        return this.updateTime;
    }
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }
}
