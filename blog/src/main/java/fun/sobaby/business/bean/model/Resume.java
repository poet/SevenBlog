package fun.sobaby.business.bean.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import fun.sobaby.business.bean.enums.SexEnum;

/**
 * 数据库表[resume]的数据模型
 * 简历
 * 
 * 
 * @author 最爱吃小鱼
 * 
 */

@TableName("resume")
@SuppressWarnings("serial")
public class Resume implements Serializable {

	@TableId
	private Long id;
    // 真实姓名 
    @TableField("real_name")
    private String realName;
    // 花名 
    @TableField("nick_name")
    private String nickName;
    // 身份证 
    @TableField("idcard")
    private String idcard;
    // 性别 
    @TableField("sex")
    private SexEnum sex;
    // 出生日期 
    @TableField("birthday")
    private String birthday;
    // 地址 
    @TableField("address")
    private String address;
    // 邮箱 
    @TableField("email")
    private String email;
    // 手机 
    @TableField("mobile")
    private String mobile;
    // 职业 
    @TableField("job")
    private String job;
    // 自我介绍 
    @TableField("introduce")
    private String introduce;
    // 大学 
    @TableField("school")
    private String school;
    // 专业 
    @TableField("profession")
    private String profession;
    // 学历 
    @TableField("degree")
    private String degree;
    // 创建时间 
    @TableField("create_time")
    private java.util.Date createTime;
    // 更新时间 
    @TableField("update_time")
    private java.util.Date updateTime;
    
    /** 默认构造函数 */
    public Resume() {
    }
    
    /** 全参构造函数 */
    public Resume(String realName, String nickName, String idcard, SexEnum sex, String birthday, String address, String email, String mobile, String job, String introduce, String school, String profession, String degree, java.util.Date createTime, java.util.Date updateTime) {
      this.realName = realName;
      this.nickName = nickName;
      this.idcard = idcard;
      this.sex = sex;
      this.birthday = birthday;
      this.address = address;
      this.email = email;
      this.mobile = mobile;
      this.job = job;
      this.introduce = introduce;
      this.school = school;
      this.profession = profession;
      this.degree = degree;
      this.createTime = createTime;
      this.updateTime = updateTime;
    }
     
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
    public  String getRealName() {
        return this.realName;
    }
    public void setRealName(String realName) {
        this.realName = realName;
    }
    public  String getNickName() {
        return this.nickName;
    }
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
    public  String getIdcard() {
        return this.idcard;
    }
    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }
    public  SexEnum getSex() {
        return this.sex;
    }
    public void setSex(SexEnum sex) {
        this.sex = sex;
    }
    public  String getBirthday() {
        return this.birthday;
    }
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
    public  String getAddress() {
        return this.address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public  String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public  String getMobile() {
        return this.mobile;
    }
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public  String getJob() {
        return this.job;
    }
    public void setJob(String job) {
        this.job = job;
    }
    public  String getIntroduce() {
        return this.introduce;
    }
    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }
    public  String getSchool() {
        return this.school;
    }
    public void setSchool(String school) {
        this.school = school;
    }
    public  String getProfession() {
        return this.profession;
    }
    public void setProfession(String profession) {
        this.profession = profession;
    }
    public  String getDegree() {
        return this.degree;
    }
    public void setDegree(String degree) {
        this.degree = degree;
    }
    public  java.util.Date getCreateTime() {
        return this.createTime;
    }
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }
    public  java.util.Date getUpdateTime() {
        return this.updateTime;
    }
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }
}
