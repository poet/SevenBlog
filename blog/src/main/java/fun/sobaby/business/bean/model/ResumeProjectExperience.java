package fun.sobaby.business.bean.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 数据库表[resume_project_experience]的数据模型
 * 项目经历
 * 
 * 
 * @author 最爱吃小鱼
 * 
 */

@TableName("resume_project_experience")
@SuppressWarnings("serial")
public class ResumeProjectExperience implements Serializable {

	@TableId
	private Long id;
    // 简历ID 
    @TableField("resume_id")
    private Long resumeId;
    // 项目名称 
    @TableField("title")
    private String title;
    // 参与角色 
    @TableField("take_role")
    private String takeRole;
    // 参与时间 
    @TableField("take_times")
    private String takeTimes;
    // 参与内容 
    @TableField("take_content")
    private String takeContent;
    
    /** 默认构造函数 */
    public ResumeProjectExperience() {
    }
    
    /** 全参构造函数 */
    public ResumeProjectExperience(Long resumeId, String title, String takeRole, String takeTimes, String takeContent) {
      this.resumeId = resumeId;
      this.title = title;
      this.takeRole = takeRole;
      this.takeTimes = takeTimes;
      this.takeContent = takeContent;
    }
     
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
    public  Long getResumeId() {
        return this.resumeId;
    }
    public void setResumeId(Long resumeId) {
        this.resumeId = resumeId;
    }
    public  String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public  String getTakeRole() {
        return this.takeRole;
    }
    public void setTakeRole(String takeRole) {
        this.takeRole = takeRole;
    }
    public  String getTakeTimes() {
        return this.takeTimes;
    }
    public void setTakeTimes(String takeTimes) {
        this.takeTimes = takeTimes;
    }
    public  String getTakeContent() {
        return this.takeContent;
    }
    public void setTakeContent(String takeContent) {
        this.takeContent = takeContent;
    }
}
