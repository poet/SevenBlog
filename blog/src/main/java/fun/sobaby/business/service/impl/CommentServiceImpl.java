package fun.sobaby.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import fun.sobaby.business.bean.model.Comment;
import fun.sobaby.business.mapper.CommentMapper;
import fun.sobaby.business.service.CommentService;
import org.springframework.stereotype.Service;

/**
 * 
 * @author 最爱吃小鱼
 *
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {

	// 方法里面使用 baseMapper 调用
}
