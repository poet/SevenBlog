package fun.sobaby.business.bean.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author 最爱吃小鱼
 */
public class CommentDTO implements Serializable {

    /**
     * 昵称
     */
    @NotBlank
    @Length(max = 50)
    private String nickName;
    /**
     *  内容
     */
    @NotBlank
    @Length(max = 500)
    private String content;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
