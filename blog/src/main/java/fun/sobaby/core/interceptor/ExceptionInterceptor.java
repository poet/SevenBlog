package fun.sobaby.core.interceptor;

import fun.sobaby.core.bean.DataResponse;
import fun.sobaby.core.enums.CodeEnum;
import fun.sobaby.core.exception.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 处理业务异常，包装参数检验的异常信息
 *
 * @author 最爱吃小鱼
 */
@ControllerAdvice
public class ExceptionInterceptor {

    private Logger logger = LoggerFactory.getLogger("baseLog");

    /**
     * Exception异常的拦截处理
     *
     * @param request
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public DataResponse handleException(HttpServletRequest request, Exception ex) {
        String message = ex.getMessage();

        logger.error("request 500 url: {}", request.getRequestURI());
        logger.error(message, ex);
        return new DataResponse(CodeEnum.ERR_500);
    }

    /**
     * BusinessException 异常的拦截处理
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(BusinessException.class)
    @ResponseBody
    public DataResponse handleGlobalException(BusinessException ex) {
        return new DataResponse(ex.getCodeEnum());
    }

    /**
     * 抓取method参数异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public DataResponse handleConstraintViolationException(ConstraintViolationException ex){
        Set<ConstraintViolation<?>> constraintViolations = ex.getConstraintViolations();
        if(!CollectionUtils.isEmpty(constraintViolations)){
            List<ErrBean> errMessages = new ArrayList<>();
            for(ConstraintViolation constraintViolation :constraintViolations){
                errMessages.add(new ErrBean(getParamName(constraintViolation.getPropertyPath()), constraintViolation.getMessage()));
            }
            return paramFailed(errMessages);
        }
        return new DataResponse(ex.getMessage());
    }

    /**
     *
     * 抓取 @Valid @RequestBody bean 异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public DataResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException ex){
        List<ObjectError> objectErrors = ex.getBindingResult().getAllErrors();
        return paramCheckException(ex, objectErrors);
    }

    /**
     *
     * 抓取 @Valid @RequestBody bean 异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(BindException.class)
    @ResponseBody
    public DataResponse handleBindException(BindException ex){
        List<ObjectError> objectErrors = ex.getBindingResult().getAllErrors();
        return paramCheckException(ex, objectErrors);
    }

    private DataResponse paramCheckException(Exception ex, List<ObjectError> objectErrors) {
        if(!CollectionUtils.isEmpty(objectErrors)) {
            FieldError fieldError;
            List<ErrBean> errMessages = new ArrayList<>();
            for (ObjectError objectError : objectErrors) {
                if (objectError instanceof FieldError) {
                    fieldError = (FieldError)objectError;
                    errMessages.add(new ErrBean(fieldError.getField(), objectError.getDefaultMessage()));
                }
            }
            return paramFailed(errMessages);
        }
        return new DataResponse(ex.getMessage());
    }

    private DataResponse paramFailed(List<ErrBean> errMessages) {
        DataResponse dataResponse = new DataResponse(CodeEnum.ERR_PARAM_VERIFY);
        dataResponse.setData(errMessages);
        return dataResponse;
    }
    private String getParamName(Path propertyPath) {
        String paramName = null;
        for (Path.Node s : propertyPath) {
            paramName = s.getName();
        }
        return paramName;
    }

    private class ErrBean {
        private String param;
        private String errmsg;

        public ErrBean(String param, String errmsg) {
            this.param = param;
            this.errmsg = errmsg;
        }
        public String getParam() {
            return param;
        }

        public void setParam(String param) {
            this.param = param;
        }

        public String getErrmsg() {
            return errmsg;
        }

        public void setErrmsg(String errmsg) {
            this.errmsg = errmsg;
        }
    }

}
