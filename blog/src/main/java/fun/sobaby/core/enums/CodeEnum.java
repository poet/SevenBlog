package fun.sobaby.core.enums;

/**
 * @author 最爱吃小鱼
 */
public enum CodeEnum {

    SUCCESS("100", "成功"),

    ERR_ATTR("101", "AttrFilter多个同时存在时,请指定具体的class"),

    ERR_401("401", "没有登录或已失效"),
    ERR_402("402", "账号在别处登录"),
    ERR_403("403", "没有权限"),
    ERR_404("404", "接口不存在"),
    ERR_500("500", "接口异常, 请稍微重试或联系管理员"),

    ERR_PARAM_VERIFY("601", "参数校验不通过"),

    ERR_ADMIN_NONE("602", "账号不存在"),
    ERR_PASSWORD("603", "密码不正确"),

    ERR_RESUME_NONE("604", "简历不存在"),
    ERR_FORMAT_TAG("605", "文章标签格式错误"),
    ERR_EXIST_FATHER_TAG("606", "父标签不能转成子标签"),
    ERR_EXIST_SON_TAG("606", "子标签不能转成其它父标签的子标签")

    ;

    private String code;
    private String message;

    CodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
