package fun.sobaby.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import fun.sobaby.business.bean.model.Tag;

/**
 * 
 * @author 最爱吃小鱼
 *
 */
public interface TagService extends IService<Tag>{

}
