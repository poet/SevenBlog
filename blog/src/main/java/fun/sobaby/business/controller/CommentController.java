package fun.sobaby.business.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import fun.sobaby.business.bean.dto.CommentDTO;
import fun.sobaby.business.bean.enums.ShowStatusEnum;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fun.sobaby.business.bean.model.Comment;
import fun.sobaby.business.service.CommentService;

/**
 * 评论的维护
 * 
 * @author 最爱吃小鱼
 *
 */
@SuppressWarnings("ALL")
@RestController
public class CommentController {

	@Autowired
	private CommentService commentService;

	/**
	 * 评论的列表
	 *
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("/comment/list")
	public IPage<Comment> list(Page page) {
		return commentService.page(page);
	}

	/**
	 * 提交评论
	 *
	 * @param record
	 */
	@RequestMapping("/comment/edit")
	public void edit(CommentDTO record) {
		Comment comment = new Comment();
		BeanUtils.copyProperties(record, comment);
		commentService.saveOrUpdate(comment);
	}

	/**
	 * 显示评论
	 *
	 * @param record
	 */
	@RequestMapping("/comment/show")
	public void show(Long id) {
		Comment comment = new Comment();
		comment.setId(id);
		comment.setStatus(ShowStatusEnum.SHOW);
		commentService.updateById(comment);
	}

	/**
	 * 评论的详情
	 *
	 * @param id
	 * @return
	 */
	@RequestMapping("/comment/get")
	public Comment detail(Long id) {
		return commentService.getById(id);
	}

	/**
	 * 评论的删除
	 *
	 * @param id
	 */
	@RequestMapping("/comment/remove")
	public void remove(Long id) {
		commentService.removeById(id);
	}
	
}
