package fun.sobaby.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import fun.sobaby.business.bean.dto.ArticleDTO;
import fun.sobaby.business.bean.model.Article;
import fun.sobaby.business.bean.vo.ArticleVO;

/**
 * 
 * @author 最爱吃小鱼
 *
 */
public interface ArticleService extends IService<Article> {

    /**
     * 文章编辑
     *
     * @param record
     */
    void edit(ArticleDTO record);

    /**
     * 文章详情
     *
     * @param id
     * @return
     */
    ArticleVO detail(Long id);

    /**
     * 删除文章
     *
     * @param id
     */
    void removeArticle(Long id);
}
