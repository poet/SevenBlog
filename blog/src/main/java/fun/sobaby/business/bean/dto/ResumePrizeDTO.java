package fun.sobaby.business.bean.dto;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author 最爱吃小鱼
 */
public class ResumePrizeDTO implements Serializable {

    /**
     * 图片
     */
    @NotBlank
    private String coverImg;
    /**
     * 奖项名称
     */
    @NotBlank
    private String title;
    /**
     * 描述
     */
    @NotBlank
    private String description;

    public String getCoverImg() {
        return coverImg;
    }

    public void setCoverImg(String coverImg) {
        this.coverImg = coverImg;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
