package fun.sobaby.business.bean.dto;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author 最爱吃小鱼
 */
public class ResumeProjectExperienceDTO implements Serializable {

    /**
     * 项目名称
     */
    @NotBlank
    private String title;
    /**
     * 参与角色
     */
    @NotBlank
    private String takeRole;
    /**
     * 参与时间
     */
    @NotBlank
    private String takeTimes;
    /**
     * 参与内容
     */
    @NotBlank
    private String takeContent;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTakeRole() {
        return takeRole;
    }

    public void setTakeRole(String takeRole) {
        this.takeRole = takeRole;
    }

    public String getTakeTimes() {
        return takeTimes;
    }

    public void setTakeTimes(String takeTimes) {
        this.takeTimes = takeTimes;
    }

    public String getTakeContent() {
        return takeContent;
    }

    public void setTakeContent(String takeContent) {
        this.takeContent = takeContent;
    }

    @Override
    public String toString() {
        return "ResumeProjectExperienceDTO{" +
                "title='" + title + '\'' +
                ", takeRole='" + takeRole + '\'' +
                ", takeTimes='" + takeTimes + '\'' +
                ", takeContent='" + takeContent + '\'' +
                '}';
    }
}
