package fun.sobaby.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 *
 * 用在Controller方法上，表示返回值过滤value属性
 *
 * @author 最爱吃小鱼
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AttrFilters {

    AttrFilter[] value();

}
