package fun.sobaby.business.bean.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author 最爱吃小鱼
 */
public class TagDTO implements Serializable {

    private Integer parentId = -1;
    @NotNull
    private String name;
    private Integer level = 1;

    private List<TagDTO> sons;

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public List<TagDTO> getSons() {
        return sons;
    }

    public void setSons(List<TagDTO> sons) {
        this.sons = sons;
    }
}
