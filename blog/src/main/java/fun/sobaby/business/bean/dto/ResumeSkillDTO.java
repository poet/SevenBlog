package fun.sobaby.business.bean.dto;

import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author 最爱吃小鱼
 */
public class ResumeSkillDTO implements Serializable {

    /**
     * 技能名称
     */
    @NotBlank
    private String name;
    /**
     * 掌握度：0~100以内的整数
     */
    @NotNull
    @Range(min = 0, max = 100)
    private Integer master;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMaster() {
        return master;
    }

    public void setMaster(Integer master) {
        this.master = master;
    }
}
