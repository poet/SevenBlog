import fun.sobaby.Application;
import fun.sobaby.business.bean.model.Admin;
import fun.sobaby.business.service.AdminService;
import org.apache.commons.codec.digest.Md5Crypt;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author 最爱吃小鱼
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, properties = "classpath:application-dev.properties")
public class TestAdmin {

    @Autowired
    AdminService adminService;


    /**
     * 生成登录用户
     */
    @Test
    public void testCreateAdmin() {

        String salt = "$1$" + RandomStringUtils.random(5, true, true);
        String password = "hello123";
        String passwordMd5 = Md5Crypt.md5Crypt(password.getBytes(), salt);


        Admin admin = new Admin();
        admin.setUsername("seven");
        admin.setPassword(passwordMd5);
        admin.setSalt(salt);
        adminService.save(admin);


    }
}
