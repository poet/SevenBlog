package fun.sobaby.business.bean.enums;

import com.alibaba.fastjson.annotation.JSONType;
import com.baomidou.mybatisplus.annotation.EnumValue;

/**
 * 显示状态
 *
 * @author 最爱吃小鱼
 */
@JSONType(serializeEnumAsJavaBean=true)
public enum ShowStatusEnum {

    NONE(0, "隐藏"),
    SHOW(1, "显示");

    @EnumValue
    private final Integer value;
    private final String  describe;

    ShowStatusEnum(Integer value, String describe) {
        this.value = value;
        this.describe = describe;
    }

    public int getValue() {
        return value;
    }

    public String getDescribe() {
        return describe;
    }
}
