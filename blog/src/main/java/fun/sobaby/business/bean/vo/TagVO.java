package fun.sobaby.business.bean.vo;

import java.io.Serializable;

/**
 * @author 最爱吃小鱼
 */
public class TagVO implements Serializable {

    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
