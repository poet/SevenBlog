package fun.sobaby.core.interceptor;


import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import fun.sobaby.core.bean.DataResponse;
import fun.sobaby.core.enums.CodeEnum;
import fun.sobaby.core.enums.CommonEnum;
import fun.sobaby.core.enums.Unimpeded;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 判断用户是否登录的拦截器
 *
 * @author 最爱吃小鱼
 */

public class SecurityInterceptor extends HandlerInterceptorAdapter {


	private Logger logger = LoggerFactory.getLogger("baseLog");

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

		if (!(handler instanceof HandlerMethod)) {
			return true;
        }

		HandlerMethod handlerMethod = (HandlerMethod) handler;

		// 如果有当前注解 , 则不需要校验
		Unimpeded unimpeded = handlerMethod.getMethodAnnotation(Unimpeded.class);
		if (unimpeded != null) {
			return true;
		}

		// 检验是否登录
		Object value = request.getSession().getAttribute(CommonEnum.LOG_SESSION_KEY.getKey());
		if (value == null) {
			logger.info("request url: {}, 没有登录", request.getRequestURI());
			print(response, new DataResponse(CodeEnum.ERR_401));
			return false;
		}
		return true;
	}

	/**
	 * 输出json
	 * @param response
	 * @param value
	 */
	public void print(ServletResponse response, Object value) {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=UTF-8");
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.print(JSONObject.toJSONString(value));
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}
}
