package fun.sobaby.core.bean;

import fun.sobaby.core.enums.CodeEnum;

import java.io.Serializable;

/**
 * 返回数据的封装
 *
 * @author 最爱吃小鱼
 */
public class DataResponse<T> implements Serializable {

    private T data;
    private String code;
    private String message;

    /**
     * 返回的正确的数据，构造
     *
     * @param data
     */
    public DataResponse(T data) {
        this.data = data;
        this.code = CodeEnum.SUCCESS.getCode();
        this.message = CodeEnum.SUCCESS.getMessage();
    }

    /**
     * 返回错误的数据，构造
     *
     * @param codeEnum
     */
    public DataResponse(CodeEnum codeEnum) {
        this.data = null;
        this.code = codeEnum.getCode();
        this.message = codeEnum.getMessage();
    }

    /**
     * 包装
     * @param body
     * @return
     */
    public static Object of(Object body) {
        if (body instanceof DataResponse) {
            return body;
        }
        return new DataResponse<>(body);
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
