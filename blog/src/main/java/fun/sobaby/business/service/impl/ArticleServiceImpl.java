package fun.sobaby.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import fun.sobaby.business.bean.dto.ArticleDTO;
import fun.sobaby.business.bean.enums.LevelEnum;
import fun.sobaby.business.bean.model.Article;
import fun.sobaby.business.bean.model.RelateTagArticle;
import fun.sobaby.business.bean.model.Tag;
import fun.sobaby.business.bean.vo.ArticleVO;
import fun.sobaby.business.bean.vo.TagVO;
import fun.sobaby.business.mapper.ArticleMapper;
import fun.sobaby.business.mapper.TagMapper;
import fun.sobaby.business.service.ArticleService;
import fun.sobaby.business.service.RelateTagArticleService;
import fun.sobaby.business.service.TagService;
import fun.sobaby.core.enums.CodeEnum;
import fun.sobaby.core.exception.BusinessException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 
 * @author 最爱吃小鱼
 *
 */
@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper, Article> implements ArticleService {

    @Autowired
    TagMapper tagMapper;

    @Autowired
    TagService tagService;
    @Autowired
    RelateTagArticleService relateTagArticleService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void edit(ArticleDTO record) {
        // 保存文章
        Article article = new Article();
        BeanUtils.copyProperties(record, article);
        if (article.getId() == null) {
            baseMapper.insert(article);
        } else {
            baseMapper.updateById(article);
        }
        // 保存标签
        for (String tagStr : record.getTags()) {
            saveOrUpdateTag(tagStr, article.getId());
        }
    }

    /**
     * 保存文章的标签及对应关系
     *
     * @param tagStr
     * @param articleId
     */
    private void saveOrUpdateTag(String tagStr, Long articleId) {
        // 保存标签
        String[] tags = tagStr.split("/");
        if (tags == null || tags.length != 2) {
            throw new BusinessException(CodeEnum.ERR_FORMAT_TAG);
        }
        Tag fatherTag = tagService.getOne(new QueryWrapper<Tag>().eq("name", tags[0]));
        if (fatherTag == null) {
            fatherTag = new Tag();
            fatherTag.setName(tags[0]);
            fatherTag.setLevel(LevelEnum.ONE);
            tagService.save(fatherTag);
        }
        Tag sonTag = tagService.getOne(new QueryWrapper<Tag>().eq("name", tags[1]));
        if (sonTag == null) {
            sonTag = new Tag();
            sonTag.setName(tags[1]);
            sonTag.setParentId(fatherTag.getId());
            sonTag.setLevel(LevelEnum.TWO);
            tagService.save(sonTag);
        } else {
            if (sonTag.getParentId().longValue() == -1) {
                throw new BusinessException(CodeEnum.ERR_EXIST_FATHER_TAG);
            }
            if (!sonTag.getParentId().equals(fatherTag.getId())) {
                throw new BusinessException(CodeEnum.ERR_EXIST_SON_TAG);
            }
        }
        // 保存对应关系
        RelateTagArticle record = relateTagArticleService
                .getOne(new QueryWrapper<RelateTagArticle>().eq("tag_id", sonTag.getId()).eq("article_id", articleId));
        if (record == null) {
            record = new RelateTagArticle();
            record.setArticleId(articleId);
            record.setTagId(sonTag.getId());
            relateTagArticleService.save(record);
        }
    }

    @Override
    public ArticleVO detail(Long id) {
        Article article = getById(id);
        List<TagVO> tagVOList = tagMapper.selectTagsByArticleId(id);
        return ArticleVO.init(article, tagVOList);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeArticle(Long id) {
        baseMapper.deleteById(id);
        relateTagArticleService.remove(new QueryWrapper<RelateTagArticle>().eq("article_id", id));
    }
}
