package fun.sobaby.core.db;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author 最爱吃小鱼
 */
@Configuration
@EnableTransactionManagement
@MapperScan("fun.sobaby.business.mapper")
public class DBConfig {

    /**
     * 数据分页插件
     *
     * @return
     */
    @Bean
    public PaginationInterceptor getPaginationInterceptor() {
        return new PaginationInterceptor();
    }

}

