package fun.sobaby.core.exception;

import fun.sobaby.core.enums.CodeEnum;

/**
 * 业务异常
 *
 * @author 最爱吃小鱼
 */
public class BusinessException extends RuntimeException {

    private CodeEnum codeEnum;

    public BusinessException(CodeEnum codeEnum) {
        this.codeEnum = codeEnum;
    }

    public CodeEnum getCodeEnum() {
        return codeEnum;
    }

    public void setCodeEnum(CodeEnum codeEnum) {
        this.codeEnum = codeEnum;
    }
}
