import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.util.IOUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import fun.sobaby.Application;
import fun.sobaby.business.bean.enums.ShowStatusEnum;
import fun.sobaby.business.bean.model.Admin;
import fun.sobaby.business.bean.model.Comment;
import fun.sobaby.business.service.AdminService;
import fun.sobaby.business.service.CommentService;
import fun.sobaby.core.enums.CodeEnum;
import org.apache.commons.codec.digest.Md5Crypt;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author 最爱吃小鱼
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, properties = "classpath:application-dev.properties")
public class TestEnumSave {

    @Autowired
    CommentService commentService;


    /**
     * 保存评论
     */
    @Test
    public void testCreateComent() {
        Comment comment = new Comment();
        comment.setContent("hello, this is content~");
        comment.setNickName("Jony");
        comment.setStatus(ShowStatusEnum.NONE);
        commentService.save(comment);

        System.out.println(JSONObject.toJSON(comment));
    }


    @Test
    public void detailComent() {

        Comment comment = commentService.getById(1L);
        System.out.println(JSONObject.toJSON(comment));

        JSON.DEFAULT_GENERATE_FEATURE = SerializerFeature.config(JSON.DEFAULT_GENERATE_FEATURE, SerializerFeature.WriteEnumUsingName, false);

        String text = JSON.toJSONString(comment);
        System.out.println(text);
        System.out.println(JSON.toJSONString(CodeEnum.SUCCESS.ordinal()));



        Comment ct = commentService.getOne(new QueryWrapper<Comment>().eq("nick_name", "Jony"));
        System.out.println(JSONObject.toJSON(ct));
    }

    @Test
    public void testJson() {
        int features = 0;
        features |= SerializerFeature.QuoteFieldNames.getMask();
        System.out.println("1." + features);
//        features &= ~SerializerFeature.QuoteFieldNames.getMask();
//        System.out.println("11." + features);

        features |= SerializerFeature.SkipTransientField.getMask();
        System.out.println("2." + features);

//        features |= SerializerFeature.WriteEnumUsingName.getMask();
//        System.out.println("3." + features);

        features |= SerializerFeature.SortField.getMask();
        System.out.println("4." + features);


        System.out.println(JSON.DEFAULT_GENERATE_FEATURE & ~SerializerFeature.WriteEnumUsingName.mask);

    }
}
