package fun.sobaby.business.controller;

import fun.sobaby.core.enums.Unimpeded;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 首页跳转类
 *
 * @author 最爱吃小鱼
 */
@Controller
public class IndexController {

    @Unimpeded
    @GetMapping("/")
    public String index() {
        return "forward:/index.html";
    }

}
