package fun.sobaby.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import fun.sobaby.business.bean.model.ResumeSkill;
import fun.sobaby.business.mapper.ResumeSkillMapper;
import fun.sobaby.business.service.ResumeSkillService;
import org.springframework.stereotype.Service;

/**
 * 
 * @author 最爱吃小鱼
 *
 */
@Service
public class ResumeSkillServiceImpl extends ServiceImpl<ResumeSkillMapper, ResumeSkill> implements ResumeSkillService {

	// 方法里面使用 baseMapper 调用
}
