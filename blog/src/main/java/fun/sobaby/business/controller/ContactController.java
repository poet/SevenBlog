package fun.sobaby.business.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fun.sobaby.business.bean.model.Contact;
import fun.sobaby.business.service.ContactService;

/**
 * 联系我的维护
 * 
 * @author 最爱吃小鱼
 *
 */
@RestController
public class ContactController {

	@Autowired
	private ContactService contactService;

	/**
	 * 联系我的列表
	 *
	 * @param page
	 * @return
	 */
	@RequestMapping("/contact/list")
	public IPage<Contact> list(Page page) {
		return contactService.page(page);
	}

	/**
	 * 联系我的详情
	 *
	 * @param id
	 * @return
	 */
	@RequestMapping("/contact/get")
	public Contact detail(Long id) {
		return contactService.getById(id);
	}

	/**
	 * 联系我的删除
	 *
	 * @param id
	 */
	@RequestMapping("/contact/remove")
	public void remove(Long id) {
		contactService.removeById(id);
	}
	
}
