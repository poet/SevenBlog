package fun.sobaby.business.bean.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 数据库表[contact]的数据模型
 * 联系我
 * 
 * 
 * @author 最爱吃小鱼
 * 
 */

@TableName("contact")
@SuppressWarnings("serial")
public class Contact implements Serializable {

	@TableId
	private Long id;
    // 姓名 
    @TableField("name")
    private String name;
    // 邮箱 
    @TableField("email")
    private String email;
    // 标题 
    @TableField("title")
    private String title;
    // 评论内容 
    @TableField("comment")
    private String comment;
    
    /** 默认构造函数 */
    public Contact() {
    }
    
    /** 全参构造函数 */
    public Contact(String name, String email, String title, String comment) {
      this.name = name;
      this.email = email;
      this.title = title;
      this.comment = comment;
    }
     
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
    public  String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public  String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public  String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public  String getComment() {
        return this.comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }
}
