package fun.sobaby.core.enums;

/**
 * @author 最爱吃小鱼
 */
public enum  CommonEnum {

    LOG_SESSION_KEY("100", "成功");

    CommonEnum(String key, String description) {
        this.key = key;
        this.description = description;
    }

    private String key;
    private String description;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
