package fun.sobaby.business.bean.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author 最爱吃小鱼
 */
public enum SexEnum {

    MAN("男"), WOMEN("女");

    @EnumValue
    @JsonValue
    private String describe;

    SexEnum(String describe) {
        this.describe = describe;
    }

    @JsonCreator
    public static SexEnum of(String value) {
        for (SexEnum sexEnum : values()) {
            if (sexEnum.describe.equals(value)) {
                return sexEnum;
            }
        }
        return null;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    @Override
    public String toString() {
        return describe;
    }

}
