package fun.sobaby.core.annotations;

import fun.sobaby.core.enums.FilterType;

import java.lang.annotation.*;


/**
 *
 * 用在Controller方法上，表示返回值过滤value属性
 *
 * @author 最爱吃小鱼
 */
@Target(ElementType.METHOD)
@Repeatable(value = AttrFilters.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface AttrFilter {

    /**
     * 排除的属性
     *
     * @return
     */
    String[] value();

    /**
     * 包含的属性
     *
     * @return
     */
    FilterType type() default FilterType.INCLUDE;

    /**
     * 指定特定的类, 默认全部
     *
     * @return
     */
    Class<?> clazz() default ALL.class;

    class ALL {}
}
