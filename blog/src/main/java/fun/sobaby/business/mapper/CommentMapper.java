package fun.sobaby.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import fun.sobaby.business.bean.model.Comment;
/**
 * 
 * @author 最爱吃小鱼
 *
 */
public interface CommentMapper extends BaseMapper<Comment> {

}
