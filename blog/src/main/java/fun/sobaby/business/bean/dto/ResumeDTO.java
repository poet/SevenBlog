package fun.sobaby.business.bean.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.annotation.JSONType;
import fun.sobaby.business.bean.enums.SexEnum;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author 最爱吃小鱼
 */
public class ResumeDTO implements Serializable {

    /**
     * 真实姓名
     */
    @NotBlank
    private String realName;
    /**
     * 花名
     */
    @NotBlank
    private String nickName;
    /**
     * 身份证
     */
    @NotBlank
    private String idcard;
    /**
     * 性别
     */
    @NotNull
    private SexEnum sex;
    /**
     * 出生日期
     */
    @NotBlank
    private String birthday;
    /**
     * 地址
     */
    @NotBlank
    private String address;
    /**
     * 邮箱
     */
    @NotBlank
    private String email;
    /**
     * 手机
     */
    @NotBlank
    private String mobile;
    /**
     * 职业
     */
    @NotBlank
    private String job;
    /**
     * 自我介绍
     */
    @NotBlank
    private String introduce;
    /**
     * 大学
     */
    @NotBlank
    private String school;
    /**
     * 专业
     */
    @NotBlank
    private String profession;
    /**
     * 学历
     */
    @NotBlank
    private String degree;

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public SexEnum getSex() {
        return sex;
    }

    public void setSex(SexEnum sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }
}
