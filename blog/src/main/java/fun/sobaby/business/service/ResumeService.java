package fun.sobaby.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import fun.sobaby.business.bean.dto.ResumeDetailDTO;
import fun.sobaby.business.bean.model.Resume;
import fun.sobaby.business.bean.vo.ResumeVO;

/**
 * 
 * @author 最爱吃小鱼
 *
 */
public interface ResumeService extends IService<Resume>{

    /**
     * 取得简历的详情信息
     *
     * @return
     */
    ResumeVO selectDetail();

    /**
     * 保存或更新简历
     *
     * @param record
     */
    void saveOrUpdateResume(ResumeDetailDTO record);
}
