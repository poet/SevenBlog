package fun.sobaby.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import fun.sobaby.business.bean.model.RelateTagArticle;
import fun.sobaby.business.mapper.RelateTagArticleMapper;
import fun.sobaby.business.service.RelateTagArticleService;
import org.springframework.stereotype.Service;

/**
 * 
 * @author 最爱吃小鱼
 *
 */
@Service
public class RelateTagArticleServiceImpl extends ServiceImpl<RelateTagArticleMapper, RelateTagArticle> implements RelateTagArticleService {

	// 方法里面使用 baseMapper 调用
}
