package fun.sobaby.business.bean.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * @author 最爱吃小鱼
 */
public class ResumeDetailDTO implements Serializable {

    /**
     * 基本的简历信息
     */
    @Valid
    @NotNull
    private ResumeDTO resume;

    /**
     * 技能 : 自然语言，包括国语、方言、英语
     */
    @Valid
    @NotNull
    @Size(min = 1)
    private List<ResumeSkillDTO> natureLanguages;
    /**
     * 技能 : 机器语言，java、go、javascript、shell, vue
     */
    @Valid
    @NotNull
    @Size(min = 1)
    private List<ResumeSkillDTO> machineLanguages;

    /**
     * 技能 : 框架 spring mybatis dubbo
     */
    @Valid
    @NotNull
    @Size(min = 1)
    private List<ResumeSkillDTO> framworks;
    /**
     * 技能 : 其它
     */
    @Valid
    @NotNull
    @Size(min = 1)
    private List<ResumeSkillDTO> others;

    /**
     * 项目经历
     */
    @Valid
    @NotNull
    @Size(min = 1)
    private List<ResumeProjectExperienceDTO> projectExperiences;

    /**
     * 工作经历
     */
    @Valid
    @NotNull
    @Size(min = 1)
    private List<ResumeWorkExperienceDTO> workExperiences;

    /**
     * 奖项列表
     */
    @Valid
    @NotNull
    @Size(min = 1)
    private List<ResumePrizeDTO> prizes;

    public ResumeDTO getResume() {
        return resume;
    }

    public void setResume(ResumeDTO resume) {
        this.resume = resume;
    }

    public List<ResumeSkillDTO> getNatureLanguages() {
        return natureLanguages;
    }

    public void setNatureLanguages(List<ResumeSkillDTO> natureLanguages) {
        this.natureLanguages = natureLanguages;
    }

    public List<ResumeSkillDTO> getMachineLanguages() {
        return machineLanguages;
    }

    public void setMachineLanguages(List<ResumeSkillDTO> machineLanguages) {
        this.machineLanguages = machineLanguages;
    }

    public List<ResumeSkillDTO> getFramworks() {
        return framworks;
    }

    public void setFramworks(List<ResumeSkillDTO> framworks) {
        this.framworks = framworks;
    }

    public List<ResumeSkillDTO> getOthers() {
        return others;
    }

    public void setOthers(List<ResumeSkillDTO> others) {
        this.others = others;
    }

    public List<ResumeProjectExperienceDTO> getProjectExperiences() {
        return projectExperiences;
    }

    public void setProjectExperiences(List<ResumeProjectExperienceDTO> projectExperiences) {
        this.projectExperiences = projectExperiences;
    }

    public List<ResumeWorkExperienceDTO> getWorkExperiences() {
        return workExperiences;
    }

    public void setWorkExperiences(List<ResumeWorkExperienceDTO> workExperiences) {
        this.workExperiences = workExperiences;
    }

    public List<ResumePrizeDTO> getPrizes() {
        return prizes;
    }

    public void setPrizes(List<ResumePrizeDTO> prizes) {
        this.prizes = prizes;
    }
}
